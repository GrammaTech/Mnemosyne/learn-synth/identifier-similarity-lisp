(defsystem "identifier-similarity"
  :description "API for ascertaining similarity between identifiers."
  :author "GrammaTech"
  :depends-on (:identifier-similarity/server)
  :version "0.0.0"
  :class :package-inferred-system
  :defsystem-depends-on (:asdf-package-system))
