# Description

Common LISP wrapper around the identifier-similarity API.

# Requirements

To utilize this wrapper, you will need to install the
identifier-similarity python package as described under
the "Client Install" instructions in the README for that
[repository](https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/identifier-similarity).
An `identifier-similarity-server` script must be on the
client's $PATH.

In short, you will need to execute the following:

```bash
git clone git@gitlab.com:GrammaTech/Mnemosyne/learn-synth/identifier-similarity.git
cd identifier-similarity
python3 setup.py bdist_wheel --universal
pip3 install dist/*
```

# Usage

To utilize the package, `(ql:register-local-projects)` to add the asd
file to your `system-index.txt` and then
`(ql:quickload :identifier-similarity)` to load the system.

The `identifier-similarity-server` python process is responsible
for performing the scoring of identifiers for similarity.  The client
may manage the server using the `start-similarity-server` and
`terminate-similarity-server` functions.  The server must be running
to accept similarity requests.

Scoring of identifiers for similarity may be performed using the
`similarity-score` function.  For instance, clients may score "foo" and
"bar" for similarity by invoking `(similarity-score "foo" "bar")`.
Additionally, clients may optionally control aspects of the scoring
by using various keyword arguments detailed in the documentation for
`similarity-score`.  For instance, to use a specific embedding in the
computation, the client may invoke the function as follows:

```
(similarity-score "foo" "bar"
                  :embeddings '("FT_CBOW_IDENTIFIER_EMBEDDING")
```
