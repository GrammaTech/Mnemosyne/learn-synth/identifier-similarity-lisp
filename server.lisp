;;; server.lisp --- api for scoring the similarity between two identifiers
(defpackage :identifier-similarity/server
  (:use :gt/full)
  (:import-from :jsown :parse :to-json)
  (:export :start-similarity-server
           :terminate-similarity-server
           :similarity-score
           :similarity-server-error))
(in-package :identifier-similarity/server)

;;; Similarity server global variables and constants.
(defvar *server* nil "Identifier similarity server process.")
(defvar *lock* (make-lock) "Lock to synchronize modifications to the server.")
(define-constant
  +identifier-similarity-server-script+ "identifier-similarity-server"
  :test #'equal
  :documentation
  "Name of the identifier similarity server executable python script.")

;;; Helper conditions and macros.
(define-condition similarity-server-error (simple-error) ()
  (:documentation "Specialization for similarity server exceptions."))

(defmacro defun-synchronized (fn args &body body)
  "Define a function FN with ARGS and BODY where the body may only
be executed by a single thread at a time."
  `(defun ,fn ,args
     (with-lock-held (*lock*)
       ,@body)))

;;; Similarity server implementation.
(defun-synchronized similarity-server-running-p ()
  "Return non-NIL if the identifier similarity server process
is currently running."
  (not (or (null *server*) (null (process-alive-p *server*)))))

(defun-synchronized start-similarity-server ()
  "If the identifier similarity server process is not running,
start the server and block until it is ready to receive requests."
  (assert (which +identifier-similarity-server-script+) nil
          "~a is not on your path" +identifier-similarity-server-script+)

  (unless (similarity-server-running-p)
    ;; Launch the server
    (setf *server*
          (launch-program (list +identifier-similarity-server-script+ "--stdio")
                          :input :stream
                          :output :stream
                          :error-output :stream))

    ;; Block until the server has loaded the embeddings and is ready
    ;; to accept requests.
    (iter (until (nest (equal "Starting identifier similarity server.")
                       (lastcar)
                       (split-sequence #\:)
                       (read-line)
                       (process-info-error-output *server*))))))

(defun-synchronized terminate-similarity-server ()
  "If the identifier similarity server process is running, terminate it."
  (when (similarity-server-running-p)
    (terminate-process *server*)
    (setf *server* nil)))

(defun-synchronized similarity-score (a b &rest args &key &allow-other-keys)
  "Compute a similarity score between two identifiers, A and B.  The
similarity score is bounded between 0.0 and 1.0 (inclusive).  Additional
optional keyword arguments controlling the score computation are detailed
below:

* EMBEDDINGS: list of embeddings to utilize when computing similarity score
  The embeddings may be one or more of:
  - 'GLOVE_WORD_EMBEDDING' - an embedding trained on English words;
        specifically the Wikipedia 2014 + Gigaword 5 datasets
  - 'FT_CBOW_IDENTIFIER_EMBEDDING' - an fast-text continuous bag of
        words embedding trained on identifiers represented as n-grams
  - 'PATH_BASED_IDENTIFIER_EMBEDDING' - an embedding of identifiers
        using a structural, tree-based representation of code
* THRESHOLD: minimum similarity score between two strings in an
  embedding below which the similarity is assumed to be 0.0.
* SPLITTER: algorithm to utilize when splitting an identifier into terms
  The splitter may be one of:
  - 'HeuristicSplitter' - split identifiers by hard delimiters, forward
        camel case, and digits, but recognize special cases such as
        `utf8`, `sha256`, etc.
  - 'PythonHeuristicSplitter' - a subclass of HeuristicSplitter with
        additional special casing for python special variables

The additional keyword arguments must satisfy the JSON schema defined
`similarity-score-input-schema.json`; otherwise an error, `input must
satisfy the JSON schema`, will be thrown.
"
  (assert (similarity-server-running-p) nil
          "Identifier similarity score server not currently running.")

  ;; Build the argument JSON and send it the server over stdin.
  (setf args (nest (alist-hash-table)
                   (mapcar (lambda (pair)
                             (destructuring-bind (k . v) pair
                               (cons (string-downcase (symbol-name k)) v))))
                   (plist-alist args)))
  (setf (gethash "a" args) a)
  (setf (gethash "b" args) b)
  (format (process-info-input *server*) "~a~%" (to-json args))
  (force-output (process-info-input *server*))

  ;; Read the response (normal or error) from the server and return
  ;; the result to the client.
  (iter (until nil)
        (when (listen (process-info-output *server*))
          (nest (return)
                (float)
                (cdr)
                (plist-get :obj)
                (parse)
                (read-line)
                (process-info-output *server*)))
        (when (listen (process-info-error-output *server*))
          (nest (error 'similarity-server-error :format-control)
                (lastcar)
                (split-sequence #\:)
                (read-line)
                (process-info-error-output *server*)))))
